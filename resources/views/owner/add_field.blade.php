@extends('layout.mainlayout')
@section('content')
    <div class="album text-muted">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Add Fields</h3>
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="text" placeholder="Field Name" name="field-name">
                        </div>
                        <div class="form-group">
                            <p>Images</p>
                            <input type="file" name="field-image">
                        </div>
                        <div class="form-group">
                            <h2>Location</h2>
                            <!--map div-->
                            <div id="map" style="width: 300px; height: 200px;"></div>
                            <!--our form-->
                            <input type="hidden" id="lat">
                            <input type="hidden" id="lng">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Select list:</label>
                            <select class="form-control" id="field-size">
                                <option>100x300</option>
                                <option>150x400</option>
                                <option>Add Custom Size</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Add Field" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
